<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class bennerModel extends Model
{
    protected $table = 'benneraplikasi';
    protected $fillable = [
        'foto'
    ];
}
