@extends('layouts.app', ['title' => __('informasi')])

@section('content')
@include('users.partials.header', ['title' => __('Informasi')])

<div class="container-fluid mt--7">
    <div class="row">
        <div class="col-xl-12 order-xl-1">
            <div class="card bg-secondary shadow">
                <div class="card-header bg-white border-0">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">{{ __('Tambah Informasi') }}</h3>
                        </div>
                        <div class="col-4 text-right">
                            <a href="{{ route('index.informasi') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form method="post" enctype="multipart/form-data" action="{{ route('create.postcreate') }}" autocomplete="off">
                        @csrf

                        <h6 class="heading-small text-muted mb-4">{{ __('Informasi') }}</h6>
                        <div class="pl-lg-4">
                            <div class="form-group{{ $errors->has('tempat') ? ' has-danger' : '' }}">
                                <label class="form-control-label" for="input-tempat">{{ __('Tempat') }}</label>
                                <input type="text" name="tempat" id="input-tempat" class="form-control form-control-alternative{{ $errors->has('tempat') ? ' is-invalid' : '' }}" placeholder="{{ __('tempat') }}" value="{{ old('tempat') }}" required autofocus>

                                @if ($errors->has('tempat'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('tempat') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('ukuran') ? ' has-danger' : '' }}">
                                <label class="form-control-label" for="input-ukuran">{{ __('Ukuran') }}</label>
                                <input type="text" name="ukuran" id="input-ukuran" class="form-control form-control-alternative{{ $errors->has('ukuran') ? ' is-invalid' : '' }}" placeholder="{{ __('ukuran') }}" value="{{ old('ukuran') }}" required>

                                @if ($errors->has('ukuran'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('ukuran') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('details') ? ' has-danger' : '' }}">
                                <label class="form-control-label" for="input-details">{{ __('Details') }}</label>
                                <input type="text" name="details" id="input-details" class="form-control form-control-alternative{{ $errors->has('details') ? ' is-invalid' : '' }}" placeholder="{{ __('details') }}" value="" required>

                                @if ($errors->has('details'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('details') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('foto') ? ' has-danger' : '' }}">
                                <label class="form-control-label" for="input-foto">{{ __('Foto') }}</label>
                                <input type="file" name="foto" id="input-foto" class="form-control form-control-alternative{{ $errors->has('foto') ? ' is-invalid' : '' }}" placeholder="{{ __('foto') }}" required>

                                @if ($errors->has('foto'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('foto') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="text-center">
                                <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.footers.auth')
</div>
@endsection