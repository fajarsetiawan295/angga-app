<?php

namespace App\Http\Controllers;

use App\bennerModel;
use Illuminate\Http\Request;
use Validator;

class bennerwebcontroller extends Controller
{
    public function index()
    {
        # code...
        $data = bennerModel::all();

        return view(
            'benner.index',
            [
                'list' => $data,
            ]
        );
    }

    public function create()
    {
        # code...
        return view('benner.create');
    }

    public function postcreate(Request $request)
    {
        Validator::make($request->all(), [
            'foto' => 'required|file',
        ]);

        // dd($request->file('foto'));
        $file = $request->file('foto');
        $imagePath = '/assets/img/';
        $path = public_path() . $imagePath;
        $extension = $file->getClientOriginalExtension();
        $filename = 'gambar-' . time() . '.' . $extension;
        $request->file('foto')->move($path, $filename);

        $input =  bennerModel::create([
            'foto' => $imagePath . $filename

        ]);

        // dd($input);

        if ($input) {
            return redirect()->route('index.benner')->withStatus(__('benner successfully created.'));
        } else {
            return redirect()->route('create.benner')->withStatus(__('benner Failed created.'));
        }
    }

    public function bennerdelet($id)
    {
        $id = decrypt($id);

        $benner = bennerModel::find($id);
        $benner->delete();

        return redirect()->route('index.benner')->withStatus(__('benner successfully created.'));
        # code...
    }
}
