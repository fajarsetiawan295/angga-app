<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'Profile';
    protected $fillable = [
            'id_user', 'name', 'alamat', 'telpon', 'nik', 'nomor_kamar', 'lantai', 'jenis_kelamin', 'foto'
    ];

    public function User()
    {
        return $this->belongsTo(User::class, 'id_user');
    }


}
