@extends('layouts.app')

@section('content')
@include('users.partials.header')

<div class="container-fluid mt--7">
    <div class="row mt-5">
        <div class="col-xl-12 mb-5 mb-xl-0">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0">Informasi Apartement</h3>
                        </div>
                        <div class="col-4 text-right">
                            <a href="{{ route('create.informasi') }}" class="btn btn-sm btn-primary">{{ __('Add Informasi') }}</a>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    @if (session('status'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('status') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                </div>
                <div class="table-responsive">
                    <!-- Projects table -->
                    <table class="table align-items-center table-flush">
                        <base-pagination :page-count="10" size="sm">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">Tempat</th>
                                    <th scope="col">Ukuran</th>
                                    <th scope="col">Foto</th>
                                    <!-- <th scope="col">Status</th> -->
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                $count = 0;
                                @endphp
                                @foreach($list as $list)
                                @php
                                $count += 1;
                                @endphp
                                <tr>
                                    <th scope="row">
                                        {{$list->tempat ?? ''}}
                                    </th>
                                    <td>
                                        {{$list->ukuran ?? ''}}
                                    </td>
                                    <td>

                                        <img src="{{ asset($list->foto ?? '') }}" class="img-thumbnail">

                                    </td>
                                    <td>
                                        <!-- <a href="{{ route('detail.penghuni', encrypt($list->id)) }}" class="btn btn-sm btn-primary">Detail</a> -->
                                        <a href="{{ route('delete.informasi', encrypt($list->id)) }}" class="btn btn-sm btn-danger">hapus</a>
                                        <!-- <a href="{{ route('edit.status', encrypt($list->id)) }}" class="btn btn-sm btn-success">edit</a> -->

                                    </td>
                                </tr>
                                @endforeach
                            </tbody>


                    </table>
                    </base-pagination>
                </div>
            </div>
        </div>

    </div>

    @include('layouts.footers.auth')
</div>
@endsection

@push('js')
<script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
<script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush