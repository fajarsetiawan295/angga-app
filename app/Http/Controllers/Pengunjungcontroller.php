<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use Validator;
use App\User;
use App\Pengunjung;

class Pengunjungcontroller extends Controller
{
    public function CreatePengunjung(Request $request)
    {
        Validator::make($request->all(), [
            'id_user' => 'required|string',
            'name' => 'required|string',
            'nik' => 'required|string',
            'tgl_kunjungan' => 'required|string',
            'alamat' => 'required|string',
            'telpon' => 'required|string',
            'lantai' => 'required|string',
            'nomor_kamar' => 'required|string',
            'jenis_kelamin' => 'required|string',
            'keperluan' => 'required|string',
            'foto' => 'required|string',
        ]);

        $user_exists = User::where('id', $request->id_user)->first();
        if ($user_exists) {

            $file = $request->file('foto');
            $imagePath = '/assets/img/';
            $path = public_path() . $imagePath;
            $extension = $file->getClientOriginalExtension();
            $filename = 'gambar-' . time() . '.' . $extension;
            $request->file('foto')->move($path, $filename);


            $user = Pengunjung::create([
                'id_user' => $request->id_user,
                'name' => $request->name,
                'nik' => $request->nik,
                'tgl_kunjungan' => $request->tgl_kunjungan,
                'alamat' => $request->alamat,
                'Telpon' => $request->telpon,
                'lantai' => $request->lantai,
                'nomor_kamar' => $request->nomor_kamar,
                'jenis_kelamin' => $request->jenis_kelamin,
                'keperluan' => $request->keperluan,
                'tgl_kunjungan' => $request->tgl_kunjungan,
                'foto' => $imagePath . $filename
            ]);

            if ($user) {
                return response()->json([
                    'status' => 200,
                    'message' => 'berhasil menambahkan Pengunjung',
                    'data' => $user
                ], 200);
            }

            return response()->json([
                'status' => 205,
                'message' => 'gagal create pengunjung'
            ], 205);
        } else {
            //User already exists
            return response()->json([
                'status' => 205,
                'message' => 'id user tidak di temukan'
            ], 205);
        }
    }

    public function GetPengunjungUser($id)
    {
        $pengunjung = Pengunjung::where('id_user', $id)->get();

        if ($pengunjung) {
            return response()->json([
                'status' => 200,
                'message' => 'berhasil',
                'data' => $pengunjung

            ], 200);
        }
        return response()->json([
            'status' => 404,
            'message' => 'gagal'
        ], 404);
    }

    public function GetdetailPengunjungUser($id)
    {
        $pengunjung = Pengunjung::where('id', $id)->first();

        if ($pengunjung) {
            return response()->json([
                'status' => 200,
                'message' => 'berhasil',
                'data' => $pengunjung

            ], 200);
        }
        return response()->json([
            'status' => 404,
            'message' => 'gagal'
        ], 404);
    }
}
