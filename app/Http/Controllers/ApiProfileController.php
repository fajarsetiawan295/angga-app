<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use Validator;
use App\User;

class ApiProfileController extends Controller
{
    public function CreateProfile(Request $request)
    {
        Validator::make($request->all(), [
            'id_user' => 'required|string',
            'name' => 'required|string',
            'alamat' => 'required|string',
            'telpon' => 'required|string',
            'nik' => 'required|string',
            'nomor_kamar' => 'required|string',
            'lantai' => 'required|string',
            'jenis_kelamin' => 'required|string',
            'foto' => 'required|string',
        ]);

        $user_exists = User::where('id', $request->id_user)->first();
        if ($user_exists) {

            if (Profile::where('id_user', $request->id_user)->first()) {

                $user =  Profile::where('id_user', $request->id_user)->update([
                    'name' => $request->name,
                    'alamat' => $request->alamat,
                    'telpon' => $request->telpon,
                    'nik' => $request->nik,
                    'nomor_kamar' => $request->nomor_kamar,
                    'lantai' => $request->lantai,
                    'jenis_kelamin' => $request->jenis_kelamin,
                ]);

                if ($user) {
                    return response()->json([
                        'status' => 200,
                        'message' => 'Successfully update'
                    ], 200);
                }
            } else {

                $file = $request->file('foto');
                $imagePath = '/assets/img/';
                $path = public_path() . $imagePath;
                $extension = $file->getClientOriginalExtension();
                $filename = 'gambar-' . time() . '.' . $extension;
                $request->file('foto')->move($path, $filename);


                $user = Profile::create([
                    'id_user' => $request->id_user,
                    'name' => $request->name,
                    'alamat' => $request->alamat,
                    'telpon' => $request->telpon,
                    'nik' => $request->nik,
                    'nomor_kamar' => $request->nomor_kamar,
                    'lantai' => $request->lantai,
                    'jenis_kelamin' => $request->jenis_kelamin,
                    'foto' => $imagePath . $filename
                ]);

                if ($user) {
                    return response()->json([
                        'status' => 200,
                        'message' => 'berhasil mendaftarkan akun'
                    ], 200);
                }
            }
        } else {
            //User already exists
            return response()->json([
                'status' => 205,
                'message' => 'id user tidak di temukan'
            ], 205);
        }
    }

    public function UpdatePhoto(Request $request)
    {
        Validator::make($request->all(), [
            'id' => 'required|string',
            'foto' => 'required|string',
        ]);

        $user_exists = User::where('id', $request->id)->first();
        if ($user_exists) {

            $file = $request->file('foto');
            $imagePath = '/assets/img/';
            $path = public_path() . $imagePath;
            $extension = $file->getClientOriginalExtension();
            $filename = 'gambar-' . time() . '.' . $extension;
            $request->file('foto')->move($path, $filename);


            $user =  Profile::where('id_user', $request->id)->update([
                'foto' => $imagePath . $filename
            ]);

            if ($user) {
                return response()->json([
                    'status' => 200,
                    'message' => 'Successfully update foto'
                ], 200);
            }
        } else {
            //User already exists
            return response()->json([
                'status' => 205,
                'message' => 'update gagal'
            ], 205);
        }
    }

    public function UpdateData(Request $request)
    {
        Validator::make($request->all(), [
            'id_user' => 'required|string',
            'name' => 'required|string',
            'alamat' => 'required|string',
            'telpon' => 'required|string',
            'nik' => 'required|string',
            'nomor_kamar' => 'required|string',
            'lantai' => 'required|string',
            'jenis_kelamin' => 'required|string',
        ]);

        $user_exists = User::where('id', $request->id_user)->first();
        if ($user_exists) {
            $user =  Profile::where('id_user', $request->id_user)->update([
                'name' => $request->name,
                'alamat' => $request->alamat,
                'telpon' => $request->telpon,
                'nik' => $request->nik,
                'nomor_kamar' => $request->nomor_kamar,
                'lantai' => $request->lantai,
                'jenis_kelamin' => $request->jenis_kelamin,
            ]);

            if ($user) {
                return response()->json([
                    'status' => 200,
                    'message' => 'Successfully update foto'
                ], 200);
            }
        } else {
            //User already exists
            return response()->json([
                'status' => 205,
                'message' => 'update gagal'
            ], 205);
        }
    }
    public function getProfile($getProfile)
    {
        $user = Profile::with('User')->where('id_user', $getProfile)->first();
        if ($user) {
            return response()->json([
                'status' => 200,
                'message' => 'data user',
                'data' =>  $user,
            ], 200);
        }
        return response()->json([
            'status' => 205,
            'message' => 'data tidak ada'
        ], 205);
    }
}
