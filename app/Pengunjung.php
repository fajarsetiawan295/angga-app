<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Profile;

class Pengunjung extends Model
{
    protected $table = 'Pengunjung';
    protected $fillable = [
        'status', 'keperluan',  'tgl_kunjungan',  'id_user', 'name', 'alamat', 'Telpon', 'nik', 'nomor_kamar', 'lantai', 'jenis_kelamin', 'foto'
    ];

    public function User()
    {
        return $this->belongsTo(User::class, 'id_user');
    }

    public function Profile()
    {
        return $this->belongsTo(Profile::class, 'id_user');
    }
}
