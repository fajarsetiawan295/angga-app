<?php

namespace App\Http\Controllers;

use App\Pengunjung;
use App\User;
use Illuminate\Http\Request;

class PengunjungwebController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Pengunjung::all();

        return view(
            'pengunjung.index',
            [
                'list' => $data,

            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $id = decrypt($id);
        // dd($id);
        $penggunjung = Pengunjung::find($id);
        // dd($penggunjung->id_user);
        $getAllToken = User::find($penggunjung->id_user);
        // dd($getAllToken);
        $data = [$getAllToken->firebase];
        fcm()
            ->to($data) // $recipients must an array
            ->priority('high')
            ->data([
                'title' => 'Tamu Anda Sudah Datang',
                'body' => 'tamu anda sudah datang silah kan ke lobi',
            ])
            ->notification([
                'title' => 'Tamu Anda Sudah Datang',
                'body' => 'tamu anda sudah datang silah kan ke lobi',
            ])
            ->send();

        $data =  $penggunjung->update([
            'status' => '1'
        ]);
        if ($data) {
            return  redirect()->route('home')->with('success', 'berhasil merubah data');
        }
        return  redirect()->route('home')->with('error', 'gagal merubah data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detailpenghuni($id)
    {
        $id = decrypt($id);
        $data = Pengunjung::with('User')->with('Profile')->find($id);
        // dd($data);

        return view(
            'pengunjung.details',
            [
                'details' => $data,

            ]
        );
    }
}
