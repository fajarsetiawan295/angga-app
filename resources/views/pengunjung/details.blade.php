@extends('layouts.app', ['title' => __('Detail Pengunjung')])

@section('content')
@include('users.partials.header', [
'title' => __('Hello'),
'description' => __('Detail Pengunjung Yang Akan Datang'),
'class' => 'col-lg-7'
])

<div class="container-fluid mt--7">
    <div class="row">
        <div class="col-xl-4 order-xl-2 mb-5 mb-xl-0">
            <div class="card card-profile shadow">
                <div class="row justify-content-center">
                    <div class="col-lg-3 order-lg-2">
                        <div class="card-profile-image">
                            <a href="#">
                                <img src="{{ asset($details->foto ?? '') }}" class="rounded-circle">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
                    <div class="d-flex justify-content-between">
                        <a href="{{ route('edit.status', encrypt($details->id ?? '')) }}" class="btn btn-sm btn-info mr-4">{{ __('Datang') }}</a>
                        <a href="{{ route('home') }}" class="btn btn-sm btn-default float-right">{{ __('Kembali') }}</a>
                    </div>
                </div>
                <div class="card-body pt-0 pt-md-4">
                    <div class="row">
                        <div class="col">
                            <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                                <!-- <div>
                                    <span class="heading">22</span>
                                    <span class="description">{{ __('Friends') }}</span>
                                </div>
                                <div>
                                    <span class="heading">10</span>
                                    <span class="description">{{ __('Photos') }}</span>
                                </div>
                                <div>
                                    <span class="heading">89</span>
                                    <span class="description">{{ __('Comments') }}</span>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <h3>
                            {{$details->name ?? ''}}
                        </h3>
                        <div class="h5 font-weight-300">
                            <i class="ni location_pin mr-2"></i>{{ $details->alamat ?? '' }}
                        </div>
                        <div class="h5 mt-4">
                            <i class="ni business_briefcase-24 mr-2"></i>Lantai - {{ $details->lantai ?? ''}}
                        </div>
                        <div>
                            <i class="ni education_hat mr-2"></i>Nomor Kamar - {{ $details->nomor_kamar ?? '' }}
                        </div>
                        <hr class="my-4" />
                        <div class="h5 mt-4">
                            <i class="ni business_briefcase-24 mr-2"></i>Nomor handphone - {{ $details->Telpon ?? ''}}
                        </div>
                        <hr class="my-4" />
                        <p>{{ $details->keperluan ?? ''}}</p>
                        <!-- <a href="#">{{ __('Show more') }}</a> -->
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-8 order-xl-1">
            <div class="card bg-secondary shadow">
                <div class="card-header bg-white border-0">
                    <div class="row align-items-center">
                        <!-- <h3 class="col-12 mb-0">{{ __('Details Penghuni') }}</h3> -->
                    </div>
                </div>
                <div class="card-body">
                    <form method="post" action="{{ route('profile.update') }}" autocomplete="off">
                        @csrf
                        @method('put')

                        <h6 class="heading-small text-muted mb-4">{{ __('Penghuni Informasi') }}</h6>

                        @if (session('status'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('status') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif

                        <div class="pl-lg-4">
                            <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                <label class="form-control-label" for="input-name">{{ __('Name') }}</label>
                                <input type="text" name="name" id="input-name" class="form-control form-control-alternative{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="{{ __('Name') }}" value="{{ $details->User->name ?? ''}}" readonly>
                            </div>
                            <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                                <label class="form-control-label" for="input-email">{{ __('Email') }}</label>
                                <input type="email" name="email" id="input-email" class="form-control form-control-alternative{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="{{ __('Email') }}" value="{{ $details->User->email ?? '' }}" readonly>
                            </div>

                            <!-- <div class="text-center">
                                <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                            </div> -->
                        </div>
                    </form>
                    <hr class="my-4" />
                    <form method="post" action="{{ route('profile.password') }}" autocomplete="off">
                        @csrf
                        @method('put')

                        <h6 class="heading-small text-muted mb-4">{{ __('Penghuni details') }}</h6>
                        <div class="pl-lg-4">
                            <div class="form-group{{ $errors->has('old_password') ? ' has-danger' : '' }}">
                                <label class="form-control-label" for="input-current-password">{{ __('Alamat Penghuni') }}</label>
                                <input type="text" name="old_password" id="input-current-password" class="form-control form-control-alternative{{ $errors->has('old_password') ? ' is-invalid' : '' }}" value="{{$details->Profile->alamat ?? ''}}" readonly>
                            </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                                <label class="form-control-label" for="input-password">{{ __('Nomor Telpon') }}</label>
                                <input type="text" name="password" id="input-password" class="form-control form-control-alternative{{ $errors->has('password') ? ' is-invalid' : '' }}" value="{{$details->Profile->telpon ?? ''}}" readonly>
                            </div>
                            <div class="form-group">
                                <label class="form-control-label" for="input-password-confirmation">{{ __('Nomor Kamar') }}</label>
                                <input type="text" name="password_confirmation" id="input-password-confirmation" class="form-control form-control-alternative" value="{{$details->Profile->nomor_kamar ?? ''}}" readonly>
                            </div>

                            <div class="form-group">
                                <label class="form-control-label" for="input-password-confirmation">{{ __('Lantai') }}</label>
                                <input type="text" name="password_confirmation" id="input-password-confirmation" class="form-control form-control-alternative" value="{{$details->Profile->lantai ?? '' }}" readonly>
                            </div>

                            <div class="text-center">
                                <!-- <button type="submit" class="btn btn-success mt-4">{{ __('Change password') }}</button> -->
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.footers.auth')
</div>
@endsection