<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::prefix('user')->group(function () {
    Route::post('/create', 'apiuser@register');
    Route::post('/login', 'apiuser@login');
    Route::post('/Update', 'apiuser@Updateapi');
    Route::get('/deleteUser/{deleteUser}', 'apiuser@deleteUser');
});

Route::prefix('Profile')->group(function () {
    Route::post('/create', 'ApiProfileController@CreateProfile');
    Route::post('/UpdatePhoto', 'ApiProfileController@UpdatePhoto');
    Route::post('/UpdateData', 'ApiProfileController@UpdateData');
    Route::get('/getProfile/{getProfile}', 'ApiProfileController@getProfile');
});

Route::prefix('Penghuni')->group(function () {
    Route::post('/create', 'ApiProfileController@CreateProfile');
    Route::post('/UpdatePhoto', 'ApiProfileController@UpdatePhoto');
    Route::post('/UpdateData', 'ApiProfileController@UpdateData');
    Route::get('/getProfile/{getProfile}', 'ApiProfileController@getProfile');
});

Route::prefix('Pengunjung')->group(function () {
    Route::post('/create', 'Pengunjungcontroller@CreatePengunjung');
    Route::post('/UpdatePhoto', 'Pengunjungcontroller@UpdatePhoto');
    Route::post('/UpdateData', 'Pengunjungcontroller@UpdateData');
    Route::get('/GetdetailPengunjungUser/{getProfile}', 'Pengunjungcontroller@GetdetailPengunjungUser');
    Route::get('/getpengunjunguser/{getProfile}', 'Pengunjungcontroller@GetPengunjungUser');
});

Route::prefix('benner')->group(function () {
    Route::get('/view', 'bennerApiController@view');
});

Route::prefix('informasi')->group(function () {
    Route::get('/view', 'informasiApiController@view');
    Route::get('/view/{id}', 'informasiApiController@details');
});
Route::get('/notifikasi/{id}', 'NotifikasiController@index');
