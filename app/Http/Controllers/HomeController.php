<?php

namespace App\Http\Controllers;

use App\Pengunjung;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        // dd($year);
        $year = date('Y');
        $month = date('m');


        $y =  Pengunjung::whereYear('created_at', $year)->count();
        $m =  Pengunjung::whereMonth('created_at', $month)->count();
        $d =  Pengunjung::whereDate('tgl_kunjungan', date("Y-m-d"))->count();
        $list =  Pengunjung::with('User')->whereDate('tgl_kunjungan', date("Y-m-d"))->where('status', null)->get();
        $k =  Pengunjung::whereDate('tgl_kunjungan', date("Y-m-d"))->where('status', 1)->count();

        // dd($list);

        return view(
            'dashboard',
            [
                'y' => $y,
                'm' => $m,
                'd' => $d,
                'k' => $k,
                'list' => $list
            ]
        );
    }
}
