<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('welcome');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
	Route::get('profile/details/{code}', 'ProfileController@detailpenghuni')->name('detail.datapenghuni');
	Route::get('profile/index', 'ProfileController@index')->name('index.datapenghuni');


	//penghuni
	Route::get('/home/{code}', 'PengunjungwebController@update')->name('edit.status');
	Route::get('/detailpenghuni/{code}', 'PengunjungwebController@detailpenghuni')->name('detail.penghuni');
	Route::get('/allpengunjung/', 'PengunjungwebController@index')->name('index.penghuni');

	//informasi
	Route::get('/informasi/', 'informasiwebcontroller@index')->name('index.informasi');
	Route::get('/create/', 'informasiwebcontroller@create')->name('create.informasi');
	Route::post('/postcreate', 'informasiwebcontroller@postcreate')->name('create.postcreate');
	Route::get('/deletinformasi/{id}', 'informasiwebcontroller@informasidelet')->name('delete.informasi');

	//benner
	Route::get('/benner/', 'bennerwebcontroller@index')->name('index.benner');
	Route::get('/createbenner/', 'bennerwebcontroller@create')->name('create.benner');
	Route::post('/postcreatebenner', 'bennerwebcontroller@postcreate')->name('createbenner.postcreate');
	Route::get('/deletbenner/{id}', 'bennerwebcontroller@bennerdelet')->name('delete.benner');
});
