<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Pengunjung extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Pengunjung', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_user');
            $table->string('name');
            $table->string('nik');
            $table->date('tgl_kunjungan');
            $table->text('alamat');
            $table->string('Telpon');
            $table->string('lantai');
            $table->string('nomor_kamar');
            $table->string('jenis_kelamin');
            $table->string('keperluan');
            $table->string('foto');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Pengunjung');
    }
}
