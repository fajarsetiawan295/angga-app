<?php

namespace App\Http\Controllers;

use App\Http\Resources\listinformasi;
use App\informasiModel;
use Illuminate\Http\Request;

class informasiApiController extends Controller
{
    public function view()
    {
        $get = informasiModel::all();

        return response()->json([
            'status' => 200,
            'message' => 'berhasil akses informasi',
            'data' => listinformasi::collection(collect($get)),
        ], 200);
    }

    public function details($id)
    {
        $get = informasiModel::find($id);


        // dd(listinformasi::collection(collect($get)));

        if ($get) {
            return response()->json([
                'status' => 200,
                'message' => 'berhasil akses informasi',
                'data' => [
                    'id' => $get->id,
                    'foto' => url('/') . $get->foto,
                    'tempat' => $get->tempat,
                    'ukuran' => $get->ukuran,
                    'details' => $get->details,
                ],
            ], 200);
        }
        return response()->json([
            'status' => 404,
            'message' => 'gagal akses informasi',
        ], 404);
    }
}
