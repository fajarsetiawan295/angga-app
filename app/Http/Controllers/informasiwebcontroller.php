<?php

namespace App\Http\Controllers;

use App\informasiModel;
use Illuminate\Http\Request;
use Validator;

class informasiwebcontroller extends Controller
{
    //
    public function index()
    {
        # code...
        $data = informasiModel::all();

        return view(
            'informasi.index',
            [
                'list' => $data,
            ]
        );
    }

    public function create()
    {
        # code...
        return view('informasi.create');
    }
    public function postcreate(Request $request)
    {
        Validator::make($request->all(), [
            'tempat' => 'required|string',
            'ukuran' => 'required|string',
            'details' => 'required|string',
            'foto' => 'required|file',
        ]);

        // dd($request->file('foto'));
        $file = $request->file('foto');
        $imagePath = '/assets/img/';
        $path = public_path() . $imagePath;
        $extension = $file->getClientOriginalExtension();
        $filename = 'gambar-' . time() . '.' . $extension;
        $request->file('foto')->move($path, $filename);

        $input =  informasiModel::create([
            'tempat' => $request->tempat,
            'ukuran' => $request->ukuran,
            'details' => $request->details,
            'foto' => $imagePath . $filename

        ]);

        // dd($input);

        if ($input) {
            return redirect()->route('index.informasi')->withStatus(__('informasi successfully created.'));
        } else {
            return redirect()->route('create.informasi')->withStatus(__('informasi Failed created.'));
        }
    }

    public function informasidelet($id)
    {
        $id = decrypt($id);

        $informasi = informasiModel::find($id);
        $informasi->delete();

        return redirect()->route('index.informasi')->withStatus(__('informasi successfully created.'));
        # code...
    }
}
