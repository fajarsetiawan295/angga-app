@extends('layouts.app')

@section('content')
@include('users.partials.header')

<div class="container-fluid mt--7">
    <div class="row mt-5">
        <div class="col-xl-12 mb-5 mb-xl-0">
            <div class="card shadow">
                <div class="card-header border-0">
                    @if (\Session::has('success'))

                    <div class="alert alert-success">
                        <base-alert type="success" dismissible>
                            <span class="alert-inner--icon"><i class="ni ni-like-2"></i></span>
                            <span class="alert-inner--text"> {!! \Session::get('success') !!}</span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </base-alert>
                    </div>
                    @endif
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0">List Penghuni</h3>
                        </div>
                        <div class="col text-right">
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <!-- Projects table -->
                    <table class="table align-items-center table-flush">
                        <base-pagination :page-count="10" size="sm">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">name</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                $count = 0;
                                @endphp
                                @foreach($list as $list)
                                @php
                                $count += 1;
                                @endphp
                                <tr>
                                    <th scope="row">
                                        {{$list->name ?? ''}}
                                    </th>
                                    <td>
                                        {{$list->email ?? ''}}
                                    </td>
                                    <td>
                                        <a href="{{ route('detail.datapenghuni', encrypt($list->id)) }}" class="btn btn-sm btn-primary">Detail</a>
                                        <!-- <a href="{{ route('edit.status', encrypt($list->id)) }}" class="btn btn-sm btn-success">Datang</a> -->

                                    </td>
                                </tr>
                                @endforeach
                            </tbody>


                    </table>
                    </base-pagination>
                </div>
            </div>
        </div>

    </div>

    @include('layouts.footers.auth')
</div>
@endsection

@push('js')
<script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
<script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush