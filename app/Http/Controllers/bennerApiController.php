<?php

namespace App\Http\Controllers;

use App\bennerModel;
use App\Http\Resources\listBenner;
use Illuminate\Http\Request;

class bennerApiController extends Controller
{
    public function view()
    {
        $get = bennerModel::all();

        return response()->json([
            'status' => 200,
            'message' => 'berhasil akses benner',
            'data' => listBenner::collection(collect($get)),
        ], 200);
    }
}
