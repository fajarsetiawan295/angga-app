<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class informasiModel extends Model
{
    protected $table = 'informasi';
    protected $fillable = [
        'foto', 'tempat',  'ukuran',  'details'
    ];
}
