<?php

namespace App\Http\Controllers;

use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
use Illuminate\Http\Request;
use App\User;

class NotifikasiController extends Controller
{
    public function index($id)
    {

        $getAllToken = User::find($id);

        $data = [$getAllToken->firebase];

        // dd($data);

        fcm()
            ->to($data) // $recipients must an array
            ->priority('high')
            ->data([
                'title' => 'Patria',
                'body' => 'selamat datang di aplikasi patria',
            ])
            ->notification([
                'title' => 'Patria',
                'body' => 'selamat datang di aplikasi patria',
            ])
            ->send();
    }
}
