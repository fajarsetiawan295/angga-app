<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;
use Auth;

class apiuser extends Controller
{
    public function register(Request $request)
    {
        Validator::make($request->all(), [
            'name' => 'required|string',
            'password' => 'required|string',
            'email' => 'required|email',
        ]);

        $user_exists = User::where('name', $request->username)->first();
        if (!$user_exists) {
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password)
            ]);

            if ($user) {
                return response()->json([
                    'status' => 200,
                    'message' => 'Successfully registered'
                ], 200);
            }
        } else {
            //User already exists
            return response()->json([
                'status' => 205,
                'message' => 'Username already exists'
            ], 205);
        }
    }
    public function Updateapi(Request $request)
    {
        Validator::make($request->all(), [
            'name' => 'required|string',
            'password' => 'required|string',
            'email' => 'required|email',
        ]);

        $user_exists = User::where('id', $request->id)->first();
        if ($user_exists) {
            $user = User::find($request->id)->update([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password)
            ]);

            if ($user) {
                return response()->json([
                    'status' => 200,
                    'message' => 'Successfully update'
                ], 200);
            }
        } else {
            //User already exists
            return response()->json([
                'status' => 205,
                'message' => 'update gagal'
            ], 205);
        }
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string',
            'password' => 'required|string',
            'token' => 'required'
        ]);

        $credentials = request(['email', 'password']);
        if (!Auth::attempt($credentials)) {
            return response()->json([
                'status' => 401,
                'message' => 'Username or password is wrong!'
            ], 401);
        }
        $user = User::with('Profile')->where('email', $request->email)->first();

        if ($user) {
            User::where('email', $request->email)->update([
                'firebase' => $request->token
            ]);
        };
        return response()->json([
            'status' => 200,
            'message' => 'Successfully login',
            'data' =>  $user,
        ], 200);
    }
    public function deleteUser($id)
    {
        $getUser = User::where('id', $id)->first();;
        if ($getUser) {
            $deleteUser = $getUser->delete($id);
            if ($deleteUser) {
                return response()->json([
                    'status' => 200,
                    'message' => 'Successfully delet akun',
                ], 200);
            }
            return response()->json([
                'status' => 404,
                'message' => 'gagal delet akun',
            ], 404);
        }
        return response()->json([
            'status' => 404,
            'message' => 'data tidak ada',
            'data' => $getUser
        ], 404);
    }
}
